package services.interfaces;

import domain.AccessToken;
import domain.LoginData;

public interface IAuthorizationService {
    AccessToken authenticate(LoginData data) throws Exception;
}
